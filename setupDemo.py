'''
Created on Jun 9, 2014

@author: hauyeung
'''
import unittest, os, shutil

class Test(unittest.TestCase):
    def setUp(self):                
        file = open('bigfile','wb')
        file.write(bytearray(b'0'*1000000))
        file.close()
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        if not os.path.exists('test1'): 
            os.mkdir('test1')     
        os.chdir('test1')   
        self.fileset = {'0','1','2'}
        for x in self.fileset:
            f = open(x,'w')
            f.close()
        os.chdir(os.path.dirname(os.path.realpath(__file__)))      
            
                    
    def test_1(self):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        self.assertEqual({'0','1','2'}, set(os.listdir('test1')))
        self.assertEqual(3,len(os.listdir('test1')))
    
    def test_3(self):  
        os.chdir(os.path.dirname(os.path.realpath(__file__)))                
        self.assertEqual(1000000, os.stat('bigfile').st_size)

    def tearDown(self):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))  
        shutil.rmtree('test1')      
        os.remove('bigfile')  
        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()